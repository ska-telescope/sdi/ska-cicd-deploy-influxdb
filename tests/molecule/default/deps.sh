#!/bin/sh

echo "Installing dependencies using shell"
echo "Current path is: ${PWD}"

echo "Installing collections:"
ansible-galaxy collection install -r ${PWD}/molecule/default/requirements.yml

echo "Installing roles:"
mkdir -p ${HOME}/.ansible/roles
cp -r ${PWD}/../playbooks/roles/* ${HOME}/.ansible/roles/
cp -r ${PWD}/../collections/ansible_collections/* ${HOME}/.ansible/roles/
ls -latr ${HOME}/.ansible/roles/

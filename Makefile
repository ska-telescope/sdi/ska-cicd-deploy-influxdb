INVENTORY_FILE ?= ./inventory_influxdb_cluster##inventory file to be generated
PRIVATE_VARS ?= ./influxdb_cluster_vars.yml##template variable for heat-cluster
ANSIBLE_USER ?= ubuntu## ansible user for the playbooks
CLUSTER_KEYPAIR ?= piers-engage## key pair available on openstack to be put in the created VMs
DEBUG ?= false
CONTAINERD ?= true
NVIDIA ?= false
IGNORE_NVIDIA_FAIL ?= false
TAGS ?=
EXTRA_VARS ?=
COLLECTIONS_PATHS ?= ./collections
V ?=

STAGE ?= test## Molecule stage name

CI_ENVIRONMENT_SLUG ?= development
CI_PIPELINE_ID ?= pipeline$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=8 count=1 2>/dev/null;echo)
CI_JOB_ID ?= job$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=4 count=1 2>/dev/null;echo)
GITLAB_USER ?= ""
CI_BUILD_TOKEN ?= ""
REPOSITORY_TOKEN ?= ""
REGISTRY_TOKEN ?= ""
GITLAB_USER_EMAIL ?= "nobody@example.com"
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
CI_APPLICATION_TAG ?= $(shell git rev-parse --verify --short=8 HEAD)
DOCKERFILE ?= Dockerfile
EXECUTOR ?= docker

# Fixed variables
TIMEOUT = 86400

.DEFAULT_GOAL := help

.PHONY: check_production

# define overides for above variables in here
-include PrivateRules.mak

ANSIBLE_COLLECTIONS_PATHS := $(COLLECTIONS_PATHS):~/.ansible/collections:/usr/share/ansible/collections
STACK_CLUSTER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/stack_cluster/playbooks
DOCKER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/docker_base/playbooks

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "PRIVATE_VARS=$(PRIVATE_VARS)"
	@echo "CLUSTER_KEYPAIR=$(CLUSTER_KEYPAIR)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE)"
	@echo "EXTRA_VARS=$(EXTRA_VARS)"
	@echo "ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "STACK_CLUSTER_PLAYBOOKS=$(STACK_CLUSTER_PLAYBOOKS)"
	@echo "DOCKER_PLAYBOOKS=$(DOCKER_PLAYBOOKS)"
	@echo "CONTAINERD=$(CONTAINERD)"
	@echo "NVIDIA=$(NVIDIA)"
	@echo "IGNORE_NVIDIA_FAIL=$(IGNORE_NVIDIA_FAIL)"
	@echo "DEBUG=$(DEBUG)"

uninstall:  # uninstall collections
	rm -rf $(COLLECTIONS_PATHS)/ansible_collections/*

install:  ## Install dependent ansible collections
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-galaxy collection install \
	-r requirements.yml -p ./collections

reinstall: uninstall install ## reinstall collections

all: build

check_production:
	(grep 'production: true' $(PRIVATE_VARS) && (echo "$(INVENTORY_FILE)" | grep "k8s_system") ) || \
	(grep 'production: false' $(PRIVATE_VARS) && (echo "$(INVENTORY_FILE)" | grep -v "k8s_system") ) || \
	(printf "aborting, as this is not production ($(PRIVATE_VARS)) \n"; exit 1)

build_vms: check_production ## Build vms only based on heat-cluster
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="cluster_keypair=$(CLUSTER_KEYPAIR)" \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
	--extra-vars="inventory_file=../../../../$(INVENTORY_FILE)" $(V)

build_nodes: check_production ## Build nodes based on heat-cluster
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="cluster_keypair=$(CLUSTER_KEYPAIR)" \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
	--extra-vars="inventory_file=../../../../$(INVENTORY_FILE)" $(V)
	make build_common
	make build_docker

build_common:  ## apply the common roles
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INVENTORY_FILE) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(PRIVATE_VARS) $(V)

build_docker:  ## apply the docker roles
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INVENTORY_FILE) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(PRIVATE_VARS) $(V)

clean_nodes:  ## destroy the nodes - CAUTION THIS DELETES EVERYTHING!!!
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/remove-cluster-infra.yml $(V)

clean: clean_nodes  ## destroy the nodes - CAUTION THIS DELETES EVERYTHING!!!

check_nodes: check_production ## Check nodes based on heat-cluster
	ansible -i ./$(INVENTORY_FILE) cluster \
	                         -m shell -a 'df; lsblk'

lint: reinstall ## Lint check playbook
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-lint -p playbooks/setup.yml \
				 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
				 $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
				 $(DOCKER_PLAYBOOKS)/docker.yml \
				 $(V) > ansible-lint-results.txt; \
	cat ansible-lint-results.txt

build_influxdb: check_production ## Build influxdb
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(INVENTORY_FILE) playbooks/setup.yml \
	-e @$(PRIVATE_VARS) \
	--extra-vars="activate_containerd=$(CONTAINERD) activate_nvidia=$(NVIDIA) ignore_nvidia_fail=$(IGNORE_NVIDIA_FAIL) debug=$(DEBUG)" $(V)

build: build_nodes build_influxdb  ## Build nodes and influxdb

# test: ## Smoke test for new created cluster
# 	ansible-playbook -i $(INVENTORY_FILE) playbooks/test.yml -e @$(PRIVATE_VARS) -vvv

link:
	rm -f tests/molecule/roles
	ln -s $(pwd)/playbooks/roles tests/molecule/roles

test: reinstall link ## run molecule tests locally outside gitlab-runner
	BASE=$$(pwd); \
	mkdir -p $${BASE}/build/reports; \
	rm -rf $${BASE}/tests/.coverage* $${BASE}/tests/.pytest_cache $${BASE}/build/reports/*; \
	if [ -n "$${MY_JOB_TOKEN}" ]; then export CI_JOB_TOKEN="$${MY_JOB_TOKEN}"; else export CI_JOB_TOKEN="$(CI_JOB_TOKEN)"; fi; \
	export ANSIBLE_CALLBACK_WHITELIST=junit; \
	export JUNIT_TASK_CLASS="True"; \
	export JUNIT_OUTPUT_DIR=$${BASE}/build/reports; \
	export CURDIR=$(CURDIR); \
	cd tests && pytest \
			--junitxml=$${BASE}/build/reports/unit-tests.xml \
			--cov \
			--cov-report term \
			--cov-report html:$${BASE}/build/reports/htmlcov \
			--cov-report xml:$${BASE}/build/reports/code-coverage.xml \
	        --log-file=$${BASE}/build/reports/pytest-logs.txt -v; \
		   RC=$$?; \
		   echo "###### pytest logs #########"; \
		   cat $${BASE}/build/reports/pytest-logs.txt; \
		   echo "RC: $${RC}"; \
           echo "###### code coverage #######"; \
           echo cat $${BASE}/build/reports/code-coverage.xml; \
 		   echo "###### unit tests #######"; \
		   cat $${BASE}/build/reports/unit-tests.xml; \
	rm -rf $${BASE}/tests/.coverage* $${BASE}/tests/.pytest_cache; \
	exit $${RC}

molecule: reinstall link ## run molecule tests but don't destroy container
	BASE=$$(pwd); \
	export ANSIBLE_ROLES_PATH=$${BASE}/playbooks/roles;  env | grep ANSIBLE; \
	export CI_JOB_TOKEN=$(CI_JOB_TOKEN); \
	cd tests && MOLECULE_NO_LOG="false" molecule --debug test --destroy=never --scenario-name=$(MOLECULE_SCENARIO_NAME)

verify: ## rerun molecule verify - must run make molecule first
	BASE=$$(pwd); \
	export ANSIBLE_COLLECTIONS_PATHS=$${BASE}/collections \
	       ANSIBLE_COLLECTIONS_PATH=$${BASE}/collections; \
	export ANSIBLE_ROLES_PATH=$${BASE}/playbooks/roles;  env | grep ANSIBLE; \
	cd tests && MOLECULE_NO_LOG="false" molecule verify --scenario-name=$(MOLECULE_SCENARIO_NAME)

destroy: ## run molecule destroy - cleanup after make molecule
	BASE=$$(pwd); \
	export ANSIBLE_COLLECTIONS_PATHS=$${BASE}/collections \
	       ANSIBLE_COLLECTIONS_PATH=$${BASE}/collections; \
	export ANSIBLE_ROLES_PATH=$${BASE}/playbooks/roles;  env | grep ANSIBLE; \
	export CI_JOB_TOKEN=$(CI_JOB_TOKEN); \
	cd tests && MOLECULE_NO_LOG="false" molecule destroy

rtest:  ## run make $(STAGE) using gitlab-runner - default: test
	BASE=$$(pwd); \
	if [ -n "$(RDEBUG)" ]; then DEBUG_LEVEL=debug; else DEBUG_LEVEL=warn; fi && \
	gitlab-runner --log-level $${DEBUG_LEVEL} exec $(EXECUTOR) \
    --docker-volumes  $(DOCKER_VOLUMES) \
    --docker-pull-policy always \
	--timeout $(TIMEOUT) \
	--env "GITLAB_USER=$(GITLAB_USER)" \
	--env "REGISTRY_TOKEN=$(REGISTRY_TOKEN)" \
	--env "CI_BUILD_TOKEN=$(CI_BUILD_TOKEN)" \
	--env "CI_JOB_TOKEN=$(CI_JOB_TOKEN)" \
	--env "MY_JOB_TOKEN=$(CI_JOB_TOKEN)" \
	--env "GIT_BASE=$(GIT_BASE)" \
	--env "TRACE=1" \
	--env "DEBUG=1" \
	$(STAGE) || true

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
